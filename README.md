# README #

### Instalation process ###
1. Create environment: pipenv --python
2. Compile and install buddy library (./configure, make && make install)
3. Add buddy links to LD_LIBRARY and run ldconfig
4. Follow the instructions on ./c_algorithms/README.md file
5. Install graphviz package
6. Run: pipenv run python homepage.py and install all the necesary modules until the program runs

### Product Backlog ###

8 20
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
2 6 1 0

3 5 2 0

4 4 3 0

5 4 1 3

6 3 4 5

7 2 6 0

8 1 0 7

9 16 0 8

### Sprint Backlog 01 ###
2 20
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 
2 1 0 1

3 16 0 2

### Sprint Backlog 02 ###
5 20
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 
2 6 1 0

3 5 2 0

4 4 3 0

5 4 1 3

6 3 4 5