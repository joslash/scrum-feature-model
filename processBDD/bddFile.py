import os
import sys
import json
import fileinput
import logging
from .configFile import ConfigFile
from .bddNode import BDDNode

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
)


class BDDFile:
    arr_lines = []
    idx_lines = -1
    lines_dict = dict()
    ansParams = dict()
    configFile = ConfigFile()

    # Private methods
    def __appendFirstLine(self):
        logging.debug('...')
        self.arr_lines.clear()

        newLine = '0' + ' ' + \
            (self.configFile.getDefaultMaxNodesValue())
        self.arr_lines.append(newLine)
        self.idx_lines = 1

    def __appendSecondLine(self):
        logging.debug('...')
        newLine = ''
        for i in range(
                int(self.configFile.getDefaultMaxNodesValue())):
            newLine += str(i) + ' '
        self.arr_lines.append(newLine)
        self.idx_lines = 2

    def __writeInitialLines(self, fileName):
        logging.debug('...__writeInitialLines...')

        self.__appendFirstLine()
        self.__appendSecondLine()

        file = open(fileName, 'w')

        for line in self.arr_lines:
            file.write(str(line) + '\n')

        file.close()

    # Public methods
    def updateMaxNodesLines(self, arr_nodes):
        logging.debug(
            'updateMaxNodesLines {}'.format(
                self.arr_lines.__len__()))
        if self.arr_lines.__len__() > 0:
            self.arr_lines[0] = (str(arr_nodes.__len__(
            ) - 2) + ' ' + str(ConfigFile.getDefaultMaxNodesValue))
        else:
            self.arr_lines.append((str(arr_nodes.__len__(
            ) - 2) + ' ' + str(ConfigFile.getDefaultMaxNodesValue)))

    def readBDDFile(self, sectionName):
        logging.debug('...readBDDFile...')
        logging.debug(sectionName)

        # If th sectionName doesnt exist in the configFile...
        try:
            open(self.configFile.getBDDFileValue(sectionName)).close()
        except BaseException:
            self.__writeInitialLines(
                self.configFile.getBDDFileValue(sectionName))

        with open(self.configFile.getBDDFileValue(sectionName)) as txt:

            self.arr_lines = []
            self.idx_lines = -1

            # First two lines contains bdd config
            conta = 1
            for line in txt.readlines():
                # Ignore empty lines
                if len(line.strip()) == 0:
                    continue

                items = line.split(' ')

                # A partir de la 3ra linea se guardan en el dictionary
                if len(items) == 4:
                    self.__addNode(
                        items[0], items[1], items[2], items[3])
                else:
                    self.arr_lines.append(line.split('\n', 1)[0])
                    self.idx_lines += 1

                conta += 1

        txt.close()

        return self.arr_lines

    def __addNode(self, node_id, node_value,
                  node_low, node_high, seconds=0):
        self.arr_lines.append(
            BDDNode(
                node_id,
                0,
                1,
                node_low,
                node_value,
                node_high,
                seconds))
        self.idx_lines += 1

    def __nodeId(self):
        logging.debug('...')

        if self.idx_lines < 2:
            # First node starts in 2
            return 2

        return str(int(self.arr_lines[self.idx_lines].node_id) + 1)

    def __incrementNodeTotals(self):
        logging.debug('...')

        nodeTotals = int(self.arr_lines[0].split(' ')[0])
        numTotals = self.arr_lines[0].split(' ')[1]
        nodeTotals += 1

        self.arr_lines[0] = str(nodeTotals) + ' ' + numTotals

    def __nodeToLine(self, line):
        logging.debug('...')

        if not hasattr(line, 'node_value'):
            return line

        return str(line.node_id) + ' ' + str(line.node_value) + \
            ' ' + str(line.node_low) + ' ' + str(line.node_high)

    def __writeBDDFile(self, sectionName):
        logging.debug('...')

        file = open(self.configFile.getBDDFileValue(sectionName), 'w')

        for line in self.arr_lines:
            file.write(self.__nodeToLine(line) + '\n')

        file.close()

    def addNewNode(self, sectionName, node_low,
                   node_value, node_high, seconds):
        logging.debug('...addNewNode...')

        self.__addNode(
            self.__nodeId(),
            node_value,
            node_low,
            node_high,
            seconds)
        logging.debug('...arr_lines...{}'.format(self.arr_lines[0]))
        self.__incrementNodeTotals()
        self.__writeBDDFile(sectionName)

        return self.arr_lines

    def deleteBDDFile(self, sectionName):
        logging.debug('...')

        try:
            os.remove(self.configFile.getBDDFileValue(sectionName))
        except BaseException:
            logging.warning('File does not exists!')

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)
