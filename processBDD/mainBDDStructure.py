import sys
import json
import fileinput
import logging
from .configFile import ConfigFile
from .bddFile import BDDFile

logging.basicConfig(
        level = logging.DEBUG,
        format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
    )

class MainBDDStructure(BDDFile):
    name = ''
    # arr_nodes = [],
    # idx_nodes = -1
    bddTree = dict()

    def __init__(self, name, arr_nodes, idx_nodes, bddTree):
        logging.debug('...')
        self.name = name
        self.arr_nodes = arr_nodes
        self.idx_nodes = idx_nodes
        self.bddTree = bddTree

    def addNode(self, node):
        logging.debug('...')
        self.idx_nodes += 1
        self.arr_nodes.append(node)

    def addNode0(self):
        logging.debug('...')
        self.addNode(0)

    def addNode1(self):
        logging.debug('...')
        self.addNode(1)

    def initialNode(self):
        logging.debug('...')

    def loadArraysFromBDD(self):
        logging.debug('...loadBDDStructure...')
        logging.debug(self.name)
        records = self.readBDDFile(self.name)
        
        logging.debug('arr_lines {}'.format(self.arr_lines))
        return self.arr_lines
    
    #Mark the Product Backlog as definitive
    def definitive(self):
        logging.debug('...')

        config = ConfigFile()
        config.productBacklog(True)

        return 'Success: [ProductBacklog] attribute was updated successfully '

    #Get definitive attribute value
    def isDefinitive(self):
        logging.debug('...isDefinitive...')

        config = ConfigFile()
        return config.isProductBacklogOpen(self.name)

    def getBDDFileName(self):
        logging.debug('...')

        config = ConfigFile()
        return config.getBDDFileValue(self.name)
