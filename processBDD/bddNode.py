import sys
import json
import logging

logging.basicConfig(
        level = logging.DEBUG,
        format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
    )

class BDDNode:
    node_id: int = 0
    node0 = -1
    node1 = -1
    node_low = -1
    node_value = -1
    node_high = -1
    seconds = -1

    def __init__(self, node_id, node0, node1, node_low, node_value, node_high, seconds):
        self.node_id = node_id
        self.node0 = node0
        self.node1 = node1
        self.node_low = node_low
        self.node_value = node_value
        self.node_high = node_high
        self.seconds = seconds

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)    