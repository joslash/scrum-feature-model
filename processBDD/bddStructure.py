from pyeda.inter import *
from graphviz import Source
from c_algorithms import ha_bdd
import numpy as np
import pydot
import sys
import json
import fileinput
from .configFile import ConfigFile

class BDDStructure:
    arrays_loaded = False
    arr_nodes = []
    idx_nodes = -1
    arr_lines = []
    idx_lines = -1
    arr_sprints = []
    idx_sprints = -1
    lines_dict = dict()
    ansParams = dict()
    BDD_FILE = 'processBDD/data.bdd'
    DOT_FILE = 'data.dot'
    CONFIG_FILE = 'config.dat'
    MAX_NODE_VALUES = 20
    MAX_SPRINT = 10

    new_node = False

    def init(self, node0, node1, node_low, node_value, node_high, seconds):
        self.node0 = node0
        self.node1 = node1
        self.node_low = node_low
        self.node_value = node_value
        self.node_high = node_high
        self.seconds = seconds

    def addNode(self, node, modify_max_nodes=True):
        logging.debug("addNode")
        self.idx_nodes += 1
        self.arr_nodes.append(node)

        if modify_max_nodes:
            self.arr_lines[0]=(str(self.arr_nodes.__len__()-2)+' '+str(self.MAX_NODE_VALUES))

    def insertInitialNodes(self, node0=0, node1=1):
        logging.debug("insert initial nodes")
        self.arrays_loaded = False
        self.arr_nodes = []
        self.idx_nodes = -1
        self.arr_lines = []
        self.idx_lines = -1
        self.lines_dict = dict()
        self.ansParams = dict()

        self.addNode(node0, False)
        self.addNode(node1, False)

        #Generate line with max node values
        self.arr_lines.append(str(self.arr_nodes.__len__()+1)+' '+str(self.MAX_NODE_VALUES))

        rango = ''
        for i in range(self.MAX_NODE_VALUES):
            rango += str(i)+ ' '
        self.arr_lines.append(rango)
        self.idx_lines = 1

    #Reinitiate everything
    def initAll(self):
        logging.debug("init all")
#         self.arrays_loaded = False
#         self.arr_nodes = []
#         self.idx_nodes = -1
#         self.arr_lines = []
#         self.idx_lines = -1
#         self.lines_dict = dict()
#         self.ansParams = dict()
#
#         self.addNode(self.node0)
#         self.addNode(self.node1)
#
#         #Generate line with max node values
#         self.arr_lines.append(str(self.arr_nodes.__len__()+1)+' '+str(self.MAX_NODE_VALUES))
#
#         rango = ''
#         for i in range(self.MAX_NODE_VALUES):
#             rango += str(i)+ ' '
#         self.arr_lines.append(rango)
#         self.idx_lines = 1
        self.insertInitialNodes()

        file=open(self.BDD_FILE,'w')

        for line in self.arr_lines:
            file.write(str(line)+'\n')

        file.close()

    #Load all nodes from backlog bdd file
    def loadNodes(self):
        logging.debug("loading nodes")

        self.new_node = True
        records = []

        with open(self.BDD_FILE) as txt:
            #First two lines contains bdd config
            cont = 1
            for line in txt.readlines():
                if (cont > 2):
                    records.append(line.split('\n', 1)[0])
                cont += 1

        txt.close()

        logging.debug('cont '+ str(cont))
        if (cont == 1):
            logging.debug('cont == 1')
            records = []
        else:
            logging.debug("cont > 1")
            # Se llenan los arrays
            if(not self.arrays_loaded):
                self.loadBDDFile()
                self.arrays_loaded = True

            for line in self.arr_nodes:
                logging.debug(line)

            records = [self.replaceIndexInLine(line) for line in records]

        self.ansParams.update({'records':records})
        self.ansParams.update({'isBacklogDef':self.isBacklogDefinitive()})
        self.ansParams.update({'maxSprint':self.MAX_SPRINT})

        return json.dumps(self.ansParams)

    #Create a new node in backlog bdd file
    def createNode(self):
        logging.debug("creating node")

        self.new_node = True
        records = []

        with open(self.BDD_FILE) as txt:
            #First two lines contains bdd config
            cont = 1
            for line in txt.readlines():
                if (cont > 2):
                    records.append(line.split('\n', 1)[0])
                cont += 1

        txt.close()

        # Se llenan los arrays
        if(not self.arrays_loaded):
            self.loadBDDFile()
            self.arrays_loaded = True

        for line in self.arr_nodes:
            logging.debug(line)

        records = [self.replaceIndexInLine(line) for line in records]

        return json.dumps(records)

    def replaceIndexInLine(self, line):

        arr = line.split(' ', -1)

        if(len(arr) != 4):
            return line

        return arr[0] + ' ' + arr[1] + ' ' + str(self.arr_nodes[int(arr[2])]) + ' ' + str(self.arr_nodes[int(arr[3])])

    def loadBDDStructure(self, seconds):
        logging.debug("generateBDDStructure(self)")

        arrFeatures = bytearray()

        try:
            ha = ha_bdd.Py_HA_BDD()
            ha.process_bdd(self.BDD_FILE, arrFeatures)
            msg = 'Success: Se evaluo con exito el feature model'
        except KeyError as e:
            msg = 'Error: '+ str(e)
            return msg

        #Read results.features file
        rFile = open("results.features", "r", encoding="utf-8")

        #Reading Core results
        core_nodes_v = []
        core = rFile.readline()
        logging.debug(core)
        if(len(core) > 0):
            core_nodes = core.split(":").pop(1).split(",")
            # '\n' is one of the elements in core_nodes
            for it in core_nodes:
                try:
                    core_nodes_v.append(int(it))
                except:
                    #do nothing
                    logging.debug('Not a number')
            logging.debug(core_nodes_v)
            msg += '@' + 'Core nodes: ' + ''.join(str(e) for e in core_nodes_v)

        #Reading Dead results
        dead_nodes_v = []
        dead = rFile.readline()

        if(len(dead) > 0):
            dead_nodes = dead.split(":").pop(1).split(",")
            # '\n' is one of the elements in dead_nodes
            for it in dead_nodes:
                logging.debug('dead ' + it)
                try:
                    dead_nodes_v.append(int(it))
                except:
                    #do nothing
                    logging.debug('Not a number')
            logging.debug(dead_nodes_v)
            msg += '@' + 'Dead nodes: ' + ''.join(str(e) for e in dead_nodes_v)

        rFile.close()

        (graph,) = pydot.graph_from_dot_file(self.DOT_FILE)

        PNG_FILE = 'public/images/data'+seconds+'.png'
        logging.debug(PNG_FILE)
        graph.write_png(PNG_FILE)

        return msg

    def generateBDDStructure(self, node0, node1, node_low, node_value, node_high, seconds):
        logging.debug("generando BDD structure")

        # if(not self.arrays_loaded):
        #     self.loadBDDFile()
        #     self.arrays_loaded = True

        self.init(node0, node1, node_low, node_value, node_high, seconds)
        msg = self.createBDDFile()

        msg = self.loadBDDStructure(seconds)

        # arrFeatures = bytearray()
        #
        # try:
        #     ha = ha_bdd.Py_HA_BDD()
        #     ha.process_bdd(self.BDD_FILE, arrFeatures)
        #     msg = 'Success: Se evaluo con exito el feature model'
        # except KeyError as e:
        #     msg = 'Error: '+ str(e)
        #     return msg
        #
        # #Read results.features file
        # rFile = open("results.features", "r", encoding="utf-8")
        #
        # #Reading Core results
        # core_nodes_v = []
        # core = rFile.readline()
        # logging.debug(core)
        # if(len(core) > 0):
        #     core_nodes = core.split(":").pop(1).split(",")
        #     # '\n' is one of the elements in core_nodes
        #     for it in core_nodes:
        #         try:
        #             core_nodes_v.append(int(it))
        #         except:
        #             #do nothing
        #             logging.debug('Not a number')
        #     logging.debug(core_nodes_v)
        #     msg += '@' + 'Core nodes: ' + ''.join(str(e) for e in core_nodes_v)
        #
        # #Reading Dead results
        # dead_nodes_v = []
        # dead = rFile.readline()
        #
        # if(len(dead) > 0):
        #     dead_nodes = dead.split(":").pop(1).split(",")
        #     # '\n' is one of the elements in dead_nodes
        #     for it in dead_nodes:
        #         try:
        #             dead_nodes_v.append(int(it))
        #         except:
        #             #do nothing
        #             logging.debug('Not a number')
        #     logging.debug(dead_nodes_v)
        #     msg += '@' + 'Dead nodes: ' + ''.join(str(e) for e in dead_nodes_v)
        #
        # rFile.close()
        #
        # (graph,) = pydot.graph_from_dot_file(self.DOT_FILE)
        #
        # PNG_FILE = 'public/images/data'+seconds+'.png'
        # graph.write_png(PNG_FILE)

        return msg

    def createBDDFile(self):
        logging.debug("generando BDD file")

        try:
            if (self.idx_nodes == -1):
#                 self.idx_nodes += 1
#                 self.arr_nodes.append(self.node0)
#                 self.idx_nodes += 1
#                 self.arr_nodes.append(self.node1)
#
#                 #Generate line with max node values
#                 self.arr_lines.append(str(self.arr_nodes.__len__()+1)+' '+str(self.MAX_NODE_VALUES))
#
#                 rango = ''
#                 for i in range(self.MAX_NODE_VALUES):
#                     rango += str(i)+ ' '
#                 self.arr_lines.append(rango)
#                 self.idx_lines = 1
                self.insertInitialNodes()

            logging.debug('show arr_lines content')
            for it in self.arr_lines:
                logging.debug(it)

            nodo_existe = False
            for i in self.arr_nodes:
                if i == self.node_value:
                    nodo_existe = True

            if(not nodo_existe):
                # self.idx_nodes += 1
                # self.arr_nodes.append(self.node_value)
                # self.arr_lines[0]=(str(self.arr_nodes.__len__()-2)+' '+str(self.MAX_NODE_VALUES))
                self.addNode(self.node_value)

            self.lines_dict.update({'idx_node':self.idx_nodes})
            self.lines_dict.update({'node':self.node_value})
            self.lines_dict.update({'low':self.node_low})
            self.lines_dict.update({'high':self.node_high})

            self.idx_lines += 1
            linea = str(self.lines_dict['idx_node'])
            linea += ' '
            linea += str(self.lines_dict['node'])
            linea += ' '

            if(int(self.lines_dict['low']) > 1):
                linea += str(self.arr_nodes.index(self.lines_dict['low']))
            else:
                linea += str(self.lines_dict['low'])
            linea += ' '

            if(int(self.lines_dict['high']) > 1):
                linea += str(self.arr_nodes.index(self.lines_dict['high']))
            else:
                linea += str(self.lines_dict['high'])

            self.arr_lines.append(linea)


            file=open(self.BDD_FILE,'w')

            for line in self.arr_lines:
                file.write(str(line)+'\n')

            file.close()

            return 'Success: Se creo con exito el archivo bdd '
        except KeyError as e:
            logging.debug("error occured "+ str(e))
            return 'Error: '+ str(e)

    #Loading BDD file content
    def loadBDDFile(self):
        logging.debug("Loading BDD file")

        file=open(self.BDD_FILE, 'r')

        linesBDD = file.readlines()

        conta = 1
        for line in linesBDD:
            #first two lines aren't nodes
            if(conta > 2):
                logging.debug(line)
                items = line.split(' ')
                if(len(items) == 4):
                    self.init(0, 1, items[2], items[1], items[3], 0)
                    self.loadArrays()

            conta += 1

        file.close()

    #Loading BDD file content
    def loadArrays(self):
        logging.debug("Loading Arrays")
        if (self.idx_nodes == -1):
            self.insertInitialNodes()

        logging.debug('show arr_lines content')
        for it in self.arr_lines:
            logging.debug(it)

        nodo_existe = False
        for i in self.arr_nodes:
            if i == self.node_value:
                nodo_existe = True

        if(not nodo_existe):
            self.addNode(self.node_value)
#             self.arr_lines[0]=(str(self.arr_nodes.__len__()-2)+' '+str(self.MAX_NODE_VALUES))

        self.lines_dict.update({'idx_node':self.idx_nodes})
        self.lines_dict.update({'node':self.node_value})
        self.lines_dict.update({'low':self.node_low})
        self.lines_dict.update({'high':self.node_high})

        logging.debug('print node')
        logging.debug(str(self.idx_nodes) + ' ' + self.node_value + ' ' + self.node_low + ' ' + self.node_high)

        self.idx_lines += 1
        linea = str(self.lines_dict['idx_node'])
        linea += ' '
        linea += str(self.lines_dict['node'])
        linea += ' '

        linea += str(self.lines_dict['low'])
        linea += ' '

        linea += str(self.lines_dict['high'])

        self.arr_lines.append(linea)

        logging.debug('show arr_lines')
        for it in self.arr_lines:
            logging.debug(it)


    # #Mark the Product Backlog as definitive
    # def definitiveBacklog(self):
    #     logging.debug('Entering definitiveBacklog method ')

    #     config = ConfigFile()
    #     config.productBacklog(True)

    #     return 'Success: Se creo con exito el archivo bdd '

    # #Get Product Backlog's value
    # def isBacklogDefinitive(self):
    #     logging.debug('Entering isBacklogDefinitive method ')

    #     config = ConfigFile()
    #     return config.isProductBacklogOpen()

    #Create any sprint
    def createSprint(self):
        logging.debug('Enterint createSprint method')

        #Increment index by 1
        #Create sprint section in configFile
        #Assign dict into array[index]

        #Ahora va a ser necesario crear funcionalidades modulares para que las mismas resuelvan el bdd y el grafo de un backlog o un sprint
