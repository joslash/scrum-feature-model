from pyeda.inter import *
from graphviz import Source
from c_algorithms import ha_bdd
import numpy as np
import pydot
import sys
import json
import fileinput
import logging
from .configFile import ConfigFile
from .bddFile import BDDFile
from .mainBDDStructure import MainBDDStructure
from .backlogBDDStructure import BacklogBDDStructure
from .sprintBDDStructure import SprintBDDStructure

class MainStructure:
    arrays_loaded = False
    arr_nodes = []
    idx_nodes = -1
    arr_lines = []
    idx_lines = -1
    lines_dict = dict()    

    arr_backlog = []
    idx_backlog = -1

    arr_sprints = []
    idx_sprints = -1

    backlogStructure = BacklogBDDStructure('')
    sprintStructure = SprintBDDStructure('')
    configFile = ConfigFile()
    bddFile = BDDFile()

    logging.basicConfig(
        level = logging.DEBUG,
        format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
    )

    new_node = False

    def init(self, node0, node1, node_low, node_value, node_high, seconds):
        self.node0 = node0
        self.node1 = node1
        self.node_low = node_low
        self.node_value = node_value
        self.node_high = node_high
        self.seconds = seconds

    def __initSprints(self, backlogName):
        logging.debug('...')
        name = backlogName +'/SPRINT '
        for i in range(int(self.configFile.getDefaultMaxSprintValue())):
            self.arr_sprints.append(SprintBDDStructure(name+str(i+1)))
        self.idx_sprints = -1

    def __initBacklogs(self):
        logging.debug("...")
        name = "PRODUCT BACKLOG "
        for i in range(int(self.configFile.getDefaultMaxBacklogValue())):
            self.arr_backlog.append(BacklogBDDStructure(name+str(i+1)))
            self.__initSprints(name+str(i+1))
        self.idx_backlog = i

    #Updates sprints array index to the first one 
    #not marked as definitive
    def __currentSprintStructure(self):
        logging.debug('...__currentSprintStructure...')

        conta = 0
        for it in self.arr_sprints:
            self.idx_sprints = conta
            if(not it.isDefinitive()):
                return it

            conta += 1


    def __currentStructure(self):
        logging.debug('...__currentStructure...')
        #get current backlog
        bddSt = self.arr_backlog[self.idx_backlog]
        logging.debug('currentStructure {}'.format(bddSt))

        #is backlog definitive
        if not bddSt.isDefinitive():
            logging.debug('backlog is not definitive ')
            return bddSt

        logging.debug('backlog is definitive ')
        return self.__currentSprintStructure()

    def __defineNewCurrentStructure(self):
        logging.debug('...')
        bddSt = self.arr_backlog[self.idx_backlog]

        if bddSt.isDefinitive():
            self.idx_sprints += 1
            bddSt = self.arr_sprints[self.idx_sprints]
            bddSt.loadArraysFromBDD()
            return self.arr_sprints[self.idx_sprints]

    def __loadExistingBacklog(self):
        logging.debug('...')
        self.__initBacklogs()

        #Temporal assignment
        bddSt = BacklogBDDStructure('temp')
        conta = 0
        for it in self.arr_backlog:
            bddSt = it
            records = bddSt.loadArraysFromBDD()
            self.idx_backlog = conta

            conta += 1
            
        records = [self.replaceIndexInLine(line) for line in records]

        ansParams = dict()
        ansParams.update({'records':records})
        ansParams.update({'isDefinitive':bddSt.isDefinitive()})
        ansParams.update({'maxSprint':self.configFile.getDefaultMaxSprintValue()})

        return ansParams

    #Backlog and Sprints arrays must be initialized before call this function
    def __loadExistingSprints(self):
        logging.debug('...')

        #Temporal assignment
        bddSt = SprintBDDStructure('temp')
        ansArray = []
        conta = 0

        for it in self.arr_sprints:
            #bddSt = SprintBDDStructure('temp')
            bddSt = it
            if self.configFile.isSectionsExists(bddSt.name):
                if bddSt.isDefinitive():
                    logging.debug('sprint {} >> isDefinitive {}'.format(bddSt.toJSON(), bddSt.isDefinitive()))
                    ansArray.append(bddSt)

                    #self.idx_sprints = conta
            conta += 1
        return ansArray

    def __loadExistingSprintsDict(self):
        logging.debug('...')

        #Temporal assignment
        bddSt = SprintBDDStructure('temp')
        ansArray = []
        for it in self.__loadExistingSprints():
            bddSt = it
            
            records = bddSt.loadArraysFromBDD()
            if(len(records) > 0):
                records = [self.replaceIndexInLine(line) for line in records]

                tmpDict = dict()
                tmpDict.update({'name': bddSt.name.split('/')[1]})
                tmpDict.update({'fileName': bddSt.name.split('/')[1]})
                tmpDict.update({'isDefinitive':bddSt.isDefinitive()})
                tmpDict.update({'nodes':records})

                ansArray.append(tmpDict)

        return ansArray

    def __returnJSON(self):
        logging.debug('...')

    #Reinitiate everything
    def initAll(self):
        logging.debug("...")

        self.configFile.initSections()
        self.__initBacklogs()

    def loadAllStructures(self):
        logging.debug('...loadAllStructures...')

        backlog = self.__loadExistingBacklog()

        bddSt = self.__currentStructure()
        logging.debug('current St...{}'.format(bddSt.toJSON()))

        records = bddSt.loadArraysFromBDD()
        logging.debug('...records...{}'.format(records))

        currentSprint = dict()

        #if(len(records) > 0):
        records = [self.replaceIndexInLine(line) for line in records]

        currentSprint.update({'name': bddSt.name.split('/')[1]})
        currentSprint.update({'fileName': bddSt.name.split('/')[1]})
        currentSprint.update({'isDefinitive':bddSt.isDefinitive()})
        currentSprint.update({'nodes':records})

        definitiveSprints = self.__loadExistingSprintsDict()

        msg = dict()
        msg.update({'backlog': backlog})
        msg.update({'definitiveSprints': definitiveSprints})
        msg.update({'currentSprint': currentSprint})

        return json.dumps(msg)

    #Load all nodes from backlog bdd file
    def loadNodes(self):
        logging.debug("...")

        bddSt = self.__currentStructure()
        records = bddSt.loadArraysFromBDD()

        records = [self.replaceIndexInLine(line) for line in records]

        ansParams = dict()
        ansParams.update({'records':records})
        ansParams.update({'isBacklogDef':bddSt.isDefinitive()})
        ansParams.update({'maxSprint':self.configFile.getDefaultMaxSprintValue()})

        return json.dumps(ansParams)

    def replaceIndexInLine(self, line):

        if not hasattr(line, 'node_value'):
            return line

        return str(line.node_id) + ' ' + str(line.node_value) + ' ' + str(line.node_low) + ' ' + str(line.node_high)

   #Create a new node in backlog bdd file
    def createNode(self):
        logging.debug("...")

        bddSt = self.__currentStructure()
        records = bddSt.loadArraysFromBDD()
        records = [self.replaceIndexInLine(line) for line in records]

        return json.dumps(records)

    def generateTemporalSprintFile(self, bddSt):
        logging.debug('...')
        logging.debug('{}'.format(bddSt.toJSON()))

        sprintBDD = SprintBDDStructure('DEFAULT')
        #To delete temporal bdd file
        sprintBDD.deleteBDDFile('DEFAULT')
        #To init temporal bdd file
        sprintBDD.loadArraysFromBDD()
        
        definitiveSprints = self.__loadExistingSprints()

        # for i in range(len(definitiveSprints)):
        #     tmpBDDSt = definitiveSprints[i]
            
        #     bddArray = tmpBDDSt.loadArraysFromBDD()
        #     for idx in range(len(bddArray)):
        #         nd = bddArray[idx]
        #         if 'processBDD.bddNode.BDDNode' in str(type(nd)):
        #             sprintBDD.addNewNode('DEFAULT', nd.node_low, nd.node_value, nd.node_high, 0)


        for nd in bddSt.arr_lines:
            if not isinstance(nd, str):
                sprintBDD.addNewNode('DEFAULT', nd.node_low, nd.node_value, nd.node_high, 0)

        logging.debug('{}'.format(sprintBDD.toJSON()))
        return self.configFile.getBDDFileValue('DEFAULT')
   
    def loadBDDStructure(self, seconds):
        logging.debug("...loadBDDStructure...")

        bddSt = self.__currentStructure()

        if len(bddSt.arr_lines) <= 2:
            return 'No hay nodos registrados aun.'

        arrFeatures = bytearray()

        try:
            ha = ha_bdd.Py_HA_BDD()

            tmpFileName = ''
            #To identify if bddSt is Product Backlog or Sprint Backlog
            if isinstance(bddSt, BacklogBDDStructure):
                tmpFileName = self.configFile.getBDDFileValue(bddSt.name)
            else:
                tmpFileName = self.generateTemporalSprintFile(bddSt)
            logging.debug('Por evaluar... {}'.format(tmpFileName))

            ha.process_bdd(tmpFileName, arrFeatures)
            msg = 'Success: Se evaluo con exito el feature model'
        except KeyError as e:
            msg = 'Error: '+ str(e)
            return msg

        #Read results.features file
        rFile = open("results.features", "r", encoding="utf-8")

        #Reading Core results
        core_nodes_v = []
        core = rFile.readline()
        logging.debug('{}'.format(core))
        if(len(core) > 0):
            core_nodes = core.split(":").pop(1).split(",")
            # '\n' is one of the elements in core_nodes
            for it in core_nodes:
                try:
                    core_nodes_v.append(int(it))
                except:
                    #do nothing
                    logging.debug('Not a number')
            logging.debug('{}'.format(core_nodes_v))
            msg += '@' + 'Core nodes: ' + ''.join(str(e) for e in core_nodes_v)

        #Reading Dead results
        dead_nodes_v = []
        dead = rFile.readline()

        if(len(dead) > 0):
            dead_nodes = dead.split(":").pop(1).split(",")
            # '\n' is one of the elements in dead_nodes
            for it in dead_nodes:
                logging.debug('dead {}'.format(it))
                try:
                    dead_nodes_v.append(int(it))
                except:
                    #do nothing
                    logging.debug('Not a number')
            logging.debug(dead_nodes_v)
            msg += '@' + 'Dead nodes: ' + ''.join(str(e) for e in dead_nodes_v)

        rFile.close()

        (graph,) = pydot.graph_from_dot_file(self.configFile.getDefaultDOTFileValue())

        PNG_FILE = 'public/images/data'+seconds+'.png'
        logging.debug('...' + PNG_FILE + '...')
        
        graph.write_png(PNG_FILE)

        arrFileName = bddSt.name.split('/')
        if(arrFileName.__len__() > 1):
            PNG_FILE = 'public/images/'+bddSt.name.split('/')[1]+'.png'
        else:
            PNG_FILE = 'public/images/'+bddSt.name.split('/')[0]+'.png'

        # remove all the white spaces
        PNG_FILE = PNG_FILE.replace(' ', '').lower()
        graph.write_png(PNG_FILE)

        return msg

    def generateBDDStructure(self, node0, node1, node_low, node_value, node_high, seconds):
        logging.debug("...generateBDDStructure...")

        try:
            bddSt = self.__currentStructure()
            logging.debug(bddSt.name)
            records = bddSt.loadArraysFromBDD()
            records = bddSt.addNewNode(bddSt.name, node_low, node_value, node_high, seconds)
            msg = self.loadBDDStructure(seconds)

            records = [self.replaceIndexInLine(line) for line in records]

            name = ''
            if len(bddSt.name.split('/')) == 2:
                name = bddSt.name.split('/')[1]
            else:
                name = bddSt.name

            currentSprint = dict()
            currentSprint.update({'name': name})
            currentSprint.update({'fileName': 'data'+seconds+'.png'})
            currentSprint.update({'isDefinitive':bddSt.isDefinitive()})
            currentSprint.update({'nodes':records})

            rValue = dict()
            rValue.update({'currentSprint': currentSprint})
            rValue.update({'message':msg})
            
            return json.dumps(rValue)            

        except KeyError as e:
            logging.error("{}".format(str(e)))
            return 'Error: '+ str(e)

    #Mark the Product/Sprint Backlog as definitive
    def markAsDefinitive(self):
        logging.debug('...')

        bddSt = self.__currentStructure()
        logging.debug('currentStructure... {}'.format(bddSt.name))

        bddSt.markAsDefinitive()

        #Define new currentStructure
        bddSt = self.__defineNewCurrentStructure()
        
        return self.loadAllStructures()

    
