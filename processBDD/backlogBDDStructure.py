import sys
import json
import fileinput
import logging

from .mainBDDStructure import MainBDDStructure
from .configFile import ConfigFile

logging.basicConfig(
        level = logging.DEBUG,
        format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
    )

class BacklogBDDStructure(MainBDDStructure):
    def __init__(self,name):
        super().__init__(name, arr_nodes=[], idx_nodes=-1, bddTree=dict())
        super().addNode0()
        super().addNode1()
        super().updateMaxNodesLines(self.arr_nodes)

    def markAsDefinitive(self):
        logging.debug('Backlog marked as definitive')

        return self.configFile.setBacklogAsDefinitive(self.name)