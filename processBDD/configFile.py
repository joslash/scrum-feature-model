import configparser
import logging

logging.basicConfig(
        level = logging.DEBUG,
        format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
    )

class ConfigFile:
    CONFIG_FILE_NAME = 'config.dat'

    def __init__(self):
        pass

    def initSections(self):
        self.__initDefaultSection()
        self.__initProductBacklogSection()

    def __initDefaultSection(self):
        logging.debug('...')
        config = configparser.ConfigParser()
        config.read(self.CONFIG_FILE_NAME)
        config.sections()

        config['DEFAULT'] = {
            'nodes' : '20',
            'BDD_FILE' : 'processBDD/sprintTemp.bdd',
            'DOT_FILE' : 'data.dot',
            'CONFIG_FILE' : 'config.dat',
            'MAX_NODE_VALUES' : '20',
            'MAX_SPRINT' : '10',
            'MAX_BACKLOG' : '1'
        }
        with open(self.CONFIG_FILE_NAME, 'w') as configFile:
            config.write(configFile)

    def __initProductBacklogSection(self):
        logging.debug('...')
        config = configparser.ConfigParser()
        config.read(self.CONFIG_FILE_NAME)
        config.sections()

        config['PRODUCT BACKLOG 1'] = {
            'isDefinitive' : 'false',
            'bdd_file' : 'processBDD/product backlog 1.bdd'
        }
        with open(self.CONFIG_FILE_NAME, 'w') as configFile:
            config.write(configFile)

    def __initSection(self, sectionName):
        logging.debug('...')
        config = configparser.ConfigParser()
        config.read(self.CONFIG_FILE_NAME)
        config.sections()
        
        config[sectionName] = {
            'isDefinitive' : 'false',
            'isTouched': 'false',
            'BDD_FILE' : 'processBDD/'+sectionName.lower().split("/")[1]+'.bdd',
        }
        with open(self.CONFIG_FILE_NAME, 'w') as configFile:
            config.write(configFile)

    def __readSection(self, section):
        logging.debug('...')
        config = configparser.ConfigParser()
        config.read(self.CONFIG_FILE_NAME)
        config.sections()
        section_dict = dict()
        section_dict.update(config.items(section))
        return section_dict

    def getDefaultMaxBacklogValue(self):
        section_dict = self.__readSection('DEFAULT')

        return section_dict['max_backlog']

    def getDefaultMaxSprintValue(self):
        section_dict = self.__readSection('DEFAULT')

        return int(section_dict['max_sprint'])

    def getDefaultMaxNodesValue(self):
        section_dict = self.__readSection('DEFAULT')
        
        return section_dict['max_node_values']  

    def getDefaultBDDFileValue(self):
        section_dict = self.__readSection('DEFAULT')
        
        return section_dict['bdd_file']

    def getBDDFileValue(self, sectionName):
        try:
            section_dict = self.__readSection(sectionName)
        except:
            #Create new section
            self.__initSection(sectionName)
            section_dict = self.__readSection(sectionName)

        return section_dict['bdd_file']

    def getDefaultDOTFileValue(self):
        section_dict = self.__readSection('DEFAULT')
        
        return section_dict['dot_file']  

    def isProductBacklogOpen(self, name):
        logging.debug('isProductBacklogOpen method {}'.format(name)) 
        section_dict = self.__readSection(name)

        if(section_dict['isdefinitive'] == 'false'):
            return False
        return True

    def isSprintTouched(self, name):
        logging.debug('...')
        section_dict = self.__readSection(name)

        if(section_dict['istouched'] == 'false'):
            return False
        return True

    def isSectionsExists(self, sectionName):
        flag = False
        try:
            open(self.getBDDFileValue(sectionName)).close()
            flag = True
        except:
            pass
        
        return flag

    def setBacklogAsDefinitive(self, name):
        logging.debug('setBacklogAsDefinitive {}'.format(name))
        
        config = configparser.ConfigParser()
        config.read(self.CONFIG_FILE_NAME)
        config.sections()

        config.set(name, 'isdefinitive', 'true')
        logging.debug('isDefinitive {}'.format(config.get(name, 'isdefinitive')))

        with open(self.CONFIG_FILE_NAME, 'w') as configFile:
            config.write(configFile)

    def setSprintAsDefinitive(self, name):
        logging.debug('setSprintAsDefinitive {}'.format(name))
        
        config = configparser.ConfigParser()
        config.read(self.CONFIG_FILE_NAME)
        config.sections()

        config.set(name, 'isdefinitive', 'true')

        with open(self.CONFIG_FILE_NAME, 'w') as configFile:
            config.write(configFile)

    def setSprintAsTouched(self, name):
        logging.debug('setSprintAsTouched {}'.format(name))

        config = configparser.ConfigParser()
        config.read(self.CONFIG_FILE_NAME)
        config.sections()

        config.set(name, 'istouched', 'true')

        with open(self.CONFIG_FILE_NAME, 'w') as configFile:
            config.write(configFile)

    # def createGetSection(self, sectionName):
    #     logging.debug('createAndGetSection {}'.format(sectionName))
    #     try:
    #         section_dict = self.__readSection(sectionName)
    #     except:
    #         #Create new section
    #         self.__initSection(sectionName)
    #         section_dict = self.__readSection(sectionName)

    #     return section_dict
