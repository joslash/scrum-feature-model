import sys
import json
import fileinput
import logging
from .mainBDDStructure import MainBDDStructure
from .configFile import ConfigFile

class SprintBDDStructure(MainBDDStructure):
    def __init__(self,name):
        super().__init__(name, arr_nodes=[], idx_nodes=-1, bddTree=dict())

    def markAsDefinitive(self):
        logging.debug('Sprint marked as definitive')

        return self.configFile.setSprintAsDefinitive(self.name)