$(document).ready(function () {
  d = new Date().getTime();

  $("#load-backlog-fd").click(function (e) {
    console.log('load-backlog-fd ');
    $.get("/sprints/loadBacklog", {
        "seconds": d
      })
      .done(function (resp) {
        var jRecords = JSON.parse(resp);
        console.log(jRecords);
        readMainStructure(jRecords.backlog);
        iterateDefinitiveSprints(jRecords.definitiveSprints);
        displayCurrentSprint(jRecords.currentSprint);
        createNewNode();
      });
  });

  $('body').on('click', '#generate-node', function (e) {
    console.log('nuevo nodo');
    $.get("/sprints/createNode")
      .done(function (records) {
        var json = JSON.parse(records);

        var div01 = 'sprint' + '01'
        $("#form-capture")
          .html(loadBacklogExistingNodes(json) + createNewNode());

        showMessage('Creating a new node!');
      })
  });

  $('body').on('click', '#generate-fd', function (e) {
    d = new Date().getTime();
    console.log('generate-fd');
    $.post("/sprints/generateBDD", {
        "node0": $("input[name='node0']").val(),
        "node1": $("input[name='node1']").val(),
        "low": $("input[name='low']").val(),
        "nValue": $("input[name='nValue']").val(),
        "high": $("input[name='high']").val(),
        "seconds": d
      })
      .done(function (resp) {
        var jRecords = JSON.parse(resp);

        displayCurrentSprint(jRecords.currentSprint);
        createNewNode();

        var message = jRecords.message;

        arrTmp = message.split("@");
        arrMsg = arrTmp[0].split(":");

        if (arrMsg[0] == 'Success') {
          showMessage(arrMsg[1]);

          $("#nodes")
            .html('<div class="alert alert-info" role="alert">' + arrTmp[1] + '<br>' + arrTmp[2] + '</div >')
        } else {
          showMessage(arrMsg[1]);
        }
      });
    e.preventDefault();
  });


  $('body').on('click', '#bloquear-fd', function (e) {
    console.log('bloquear backlog');
    $.get("/sprints/definitiveProductBacklog")
      .done(function (records) {
        var jRecords = JSON.parse(records);

        iterateDefinitiveSprints(jRecords.definitiveSprints);
        displayCurrentSprint(jRecords.currentSprint);
        createNewNode()
      })

    e.preventDefault();
  });

  $("#generate-sprint-fd").click(function (e) {
    console.log('sprint nuevo feature diagram');

    e.preventDefault();
  });

});
//
function showMessage(msg) {
  $("#message")
    .html('<div class="alert alert-success" role="alert">' + msg + '</div >');
}
//
function loadBacklogExistingNodes(records) {
  var code = '';

  records.map(t => {
    values = t.split(' ');
    if (values.length == 4) {
      code = code + '<div class="form-row"><div class="col">' +
        '<label for="nValue">Node</label>' +
        '<input type="text" class="form-control" name="nValue' + values[1] + '" value="' + values[1] + '" placeholder="Enter node value" />' +
        '</div>' +
        '<div class="col">' +
        '<label for="nValue">Low</label>' +
        '<input type="text" class="form-control" name="low' + values[1] + '" value="' + values[2] + '" placeholder="Enter node value" />' +
        '</div>' +
        '<div class="col">' +
        '<label for="nValue">High</label>' +
        '<input type="text" class="form-control" name="high' + values[1] + '" value="' + values[3] + '" placeholder="Enter node value" />' +
        '</div></div>';
    }
  });

  return code;
}
//
function createNewNode() {
  return '<div class="form-row"><div class="col">' +
    '<label for="nValue">Node</label>' +
    '<input type="text" class="form-control" name="nValue" placeholder="Enter node value" />' +
    '</div>' +
    '<div class="col">' +
    '<label for="nValue">Low</label>' +
    '<input type="text" class="form-control" name="low" placeholder="Enter node value" />' +
    '</div>' +
    '<div class="col">' +
    '<label for="nValue">High</label>' +
    '<input type="text" class="form-control" name="high" placeholder="Enter node value" />' +
    '</div></div>';
}
//
function readMainStructure(backlog) {
  htmlCode = loadBacklogExistingNodes(backlog.records);
  $("#main-backlog-table")
    .html(htmlCode);
}
//
function iterateDefinitiveSprints(sprints) {
  console.log('iterateDefinitiveSprints ', JSON.stringify(sprints));

  sprints.map(t => {
    displaySprint(t);
  });
}
//
function displayCurrentSprint(sprint) {
  console.log('displayCurrentSprint ');

  displaySprint(sprint)
}
//
function displaySprint(sprint) {
  console.log('displaySprint ', sprint.name);
  sprintValue = sprint.name.split(' ')[1];
  code = generateHTMLCode(sprintValue, sprint);
  $("#sprint" + sprintValue).html(code);


}
//
function displayButtons() {
  code =
    '<div id="generate-btn" class="col-md-6"><button type="button" id="generate-fd" ' +
    'class="btn btn-primary mb-2">Generate</button></div> ' +
    '<div id="bloquear-btn" class="col-md-6"><button type="button" id="bloquear-fd" ' +
    'class="btn btn-primary mb-2">Bloquear</button></div> ';

  return code;
}
//
function generateHTMLCode(sprintValue, sprint) {
  nodes = sprint.nodes;

  console.log('isDefinitive ', sprint.isDefinitive);

  btns = '';
  createNode = '';

  if (!sprint.isDefinitive) {
    btns = displayButtons();
    createNode = createNewNode();
  }

  const element = '/static/images/sprint' + sprintValue + '.png' + '?t=' + new Date().getTime();


  code = '<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3"> ' +
    '<div class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden"> ' +
    '<div class="my-3 py-3"> ' +
    '<h2 class="display-5">Sprint Backlog ' + sprintValue + '</h2> ' +
    '</p> ' +
    '<div id="form-capture"' + sprintValue + '>' + iterateNodes(nodes) + '</div> ' +
    createNode +
    '<div id="form-captura-btn" class="row col-md-12"> ' +
    btns +
    '</div> ' +
    '</div> ' +
    '</div> ' +
    '<div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden"> ' +
    '<div class="my-3 p-3"> ' +
    '<h2 class="display-5">Graph</h2> ' +
    '<p class="lead"></p> ' +
    '<img id="feature-model" src="' + element + '" > ' +
    '</div> ' +
    '</div> ' +
    '</div>';

  return code;
}
//
function iterateNodes(nodes) {
  console.log('iterateNodes ', JSON.stringify(nodes));
  code = ''

  nodes?.map(n => {
    values = n.split(' ');
    if (values.length == 4) {
      code = code + '<div class="form-row"><div class="col">' +
        '<label for="nValue">Node</label>' +
        '<input type="text" class="form-control" name="nValue' + values[1] + '" value="' + values[1] + '" placeholder="Enter node value" />' +
        '</div>' +
        '<div class="col">' +
        '<label for="nValue">Low</label>' +
        '<input type="text" class="form-control" name="low' + values[1] + '" value="' + values[2] + '" placeholder="Enter node value" />' +
        '</div>' +
        '<div class="col">' +
        '<label for="nValue">High</label>' +
        '<input type="text" class="form-control" name="high' + values[1] + '" value="' + values[3] + '" placeholder="Enter node value" />' +
        '</div></div>';
    }
  });

  return code;
}
//
function refreshImage(imgElement, imgURL) {
  var timestamp = new Date().getTime();
  var el = document.getElementById(imgElement);
  var qString = "?t=" + timestamp;
  el.src = imgURL + qString;
}