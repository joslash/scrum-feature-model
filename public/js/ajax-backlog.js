$(document).ready(function () {
  d = new Date().getTime();
  
  $("#refresh").click(function (e) {
    console.log('loadNodes ');
    $.get("/backlog/loadNodes", {
        "seconds": d
      })
      .done(function (ansParams) {
        var params = JSON.parse(ansParams);
        console.log(params);
        var isBacklogDef = (params['isBacklogDef'] === true)
        var code = '';

        // console.log(n)
        console.log(isBacklogDef)

        var code = loadBacklogExistingNodes(params['records']);
        $("#form-capture")
          .html(code);

        // Si el baclog esta cerrado no se deben mostrar botones
        if (isBacklogDef){
          hideBacklogBtns();

          $("#sprint1")
            .html(createSprint());
        }
        else
          showBacklogBtns();


        showMessage('Loading nodes!');

        $("#feature-model").removeAttr("src").attr('src', '/static/images/data' + d + '.png');
      });
  });

  $("#init-all").click(function(e) {
    console.log('initAll');
     $.get("/backlog/initAll")
       .done(function (msg) {
         showMessage(msg);
       });
  })

  $("#generate-nd").click(function (e) {
    console.log('nuevo nodo');
    $.get("/backlog/createNode")
      .done(function (records) {
        var json = JSON.parse(records);

        $("#form-capture")
          .html(loadBacklogExistingNodes(json) + createNewBacklogNode());

        showMessage('Creating a new node!');
      })
  });

  $('body').on('click', '#generate-fd', function (e) {
    d = new Date().getTime();
    console.log('generate-fd');
    $.post("/backlog/generateBDD", {
        "node0": $("input[name='node0']").val(),
        "node1": $("input[name='node1']").val(),
        "low": $("input[name='low']").val(),
        "nValue": $("input[name='nValue']").val(),
        "high": $("input[name='high']").val(),
        "seconds": d
      })
      .done(function (resp) {
        var jRecords = JSON.parse(resp);
        message = jRecords.message;

        arrTmp = message.split("@");

        arrMsg = arrTmp[0].split(":");

        if (arrMsg[0] == 'Success') {
          showMessage(arrMsg[1]);

          $("#nodes")
            .html('<div class="alert alert-info" role="alert">' + arrTmp[1] + '<br>' + arrTmp[2] + '</div >')

          $("#feature-model").removeAttr("src").attr('src', '/static/images/data' + d + '.png');
        } else {
          showMessage(arrMsg[1]);

        }

      });
    e.preventDefault();
  });


  $('body').on('click', '#bloquear-fd', function (e) {
    console.log('bloquear backlog');
    $.get("/backlog/definitiveProductBacklog")
      .done(function (e) {
        console.log('bloquear...');
        $("#generate-node")
          .html('<span></span>');

          hideBacklogBtns();
      })

    e.preventDefault();
  });

  $('body').on('click', "#generate-sprint-nd", function (e) {
    console.log('sprint nuevo nodo');

    $.get("/createSprintNode")
      .done(function(e) {
        console.log('Se llamo create sprint node');

        var json = JSON.parse(records);

        $("#form-capture")
          .html(loadBacklogExistingNodes(json) + createNewBacklogNode());

        showMessage('Creating a new node!');
      });

    e.preventDefault();
  });

  $("#generate-sprint-fd").click(function (e) {
    console.log('sprint nuevo feature diagram');

    e.preventDefault();
  });

});

function showMessage(msg) {
  $("#message")
    .html('<div class="alert alert-success" role="alert">' +msg + '</div >');
}

function showBacklogBtns() {
  $("#form-captura-btn")
    .html('<div id="generate-btn" class="col-md-6"><button type="button" id="generate-fd" class="btn btn-primary mb-2">Generate</button></div>' +
      '<div id="bloquear-btn" class="col-md-6"><button type="button" id="bloquear-fd" class="btn btn-primary mb-2">Bloquear</button></div>');
}

function hideBacklogBtns() {
  $("#form-captura-btn")
    .html('<span></span>');
}

function loadBacklogExistingNodes(records) {
  var code = '';

  records.map(t => {
        values = t.split(' ');
        if (values.length == 4) {
          code = code + '<div class="form-row"><div class="col">' +
            '<label for="nValue">Node</label>' +
            '<input type="text" class="form-control" name="nValue' + values[1] + '" value="' + values[1] + '" placeholder="Enter node value" />' +
            '</div>' +
            '<div class="col">' +
            '<label for="nValue">Low</label>' +
            '<input type="text" class="form-control" name="low' + values[1] + '" value="' + values[2] + '" placeholder="Enter node value" />' +
            '</div>' +
            '<div class="col">' +
            '<label for="nValue">High</label>' +
            '<input type="text" class="form-control" name="high' + values[1] + '" value="' + values[3] + '" placeholder="Enter node value" />' +
            '</div></div>';
        }
      });

    return code;
}

function createNewBacklogNode() {
  return '<div class="form-row"><div class="col">' +
    '<label for="nValue">Node</label>' +
    '<input type="text" class="form-control" name="nValue" placeholder="Enter node value" />' +
    '</div>' +
    '<div class="col">' +
    '<label for="nValue">Low</label>' +
    '<input type="text" class="form-control" name="low" placeholder="Enter node value" />' +
    '</div>' +
    '<div class="col">' +
    '<label for="nValue">High</label>' +
    '<input type="text" class="form-control" name="high" placeholder="Enter node value" />' +
    '</div></div>';
}

function createSprint() {
  var htmlCode =
    '<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3"> ' +
    '<div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center  overflow-hidden"> ' +
    '<div class="my-3 py-3"> ' +
    '<h2 class="display-5">First Sprint</h2> ' +
    '<p class="lead"><button type="button" id="generate-sprint-nd" class="btn btn-primary mb-2">+</button></p> ' +
    '<form> ' +
    '<div id="form-capture"> ' +
    '</div> ' +
    '<button type="button" id="generate-sprint-fd" class="btn btn-primary mb-2">Generate</button> ' +
    '</form> ' +
    '</div> ' +
    '</div> ' +
    '<div class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden"> ' +
    '<div class="my-3 p-3"> ' +
    '<h2 class="display-5">Graph</h2> ' +
    '<p class="lead"></p> ' +
    '<img id="feature-model" /> ' +
    '</div> ' +
    '</div> ' +
    '</div> ';

    return htmlCode;
}

