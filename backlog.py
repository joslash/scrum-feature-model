import sys
import os.path
import cherrypy
import logging

sys.path.append("/processBDD")
from processBDD.main import MainStructure

logging.basicConfig(
        level = logging.DEBUG,
        format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
    )

class Backlog:
    bdd = MainStructure()

    def __init__(self):
        pass

    @cherrypy.expose
    def index(self):
        return open(os.path.join('./public/html', u'backlog.html'))

    @cherrypy.expose
    def initAll(self):
        logging.debug('init all')
        self.bdd.initAll()
        msg = "everything has been initialized"
        return msg

    @cherrypy.expose
    def loadNodes(self, seconds):
        logging.debug("Loading nodes")
        msg = self.bdd.loadNodes()
        self.bdd.loadBDDStructure(seconds)
        return msg

    @cherrypy.expose
    def createNode(self):
        logging.debug("Create node")
        msg = self.bdd.createNode()
        return msg

    @cherrypy.expose
    def generateBDD(self, node0=0, node1=1, low=-1, nValue=-1, high=-1, seconds=-1):
        logging.debug("Generate BDD Structure")
        msg = self.bdd.generateBDDStructure(node0, node1, low, nValue, high, seconds)
        return msg

    @cherrypy.expose
    def definitiveProductBacklog(self):
        logging.debug("Definitive product backlog")
        return self.bdd.markAsDefinitive()

    @cherrypy.expose
    def createSprintNode(self):
        logging.debug("Create sprint node")
        self.bdd.createNode()