import sys
import os.path
import cherrypy
import logging

sys.path.append("/processBDD")
from processBDD.main import MainStructure

logging.basicConfig(
        level = logging.DEBUG,
        format="%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(lineno)d :: %(message)s"
    )

class Sprint:
    bdd = MainStructure()

    def __init__(self):
        pass

    @cherrypy.expose
    def index(self):
        return open(os.path.join('./public/html', u'sprint.html'))

    @cherrypy.expose
    def loadBacklog(self, seconds):
        logging.debug("...loadBacklog...")
        msg = self.bdd.loadAllStructures()
        return msg

    @cherrypy.expose
    def createNode(self):
        logging.debug("...")
        msg = self.bdd.createNode()
        return msg

    @cherrypy.expose
    def generateBDD(self, node0=0, node1=1, low=-1, nValue=-1, high=-1, seconds=-1):
        logging.debug("...")
        msg = self.bdd.generateBDDStructure(node0, node1, low, nValue, high, seconds)
        return msg

    @cherrypy.expose
    def definitiveProductBacklog(self):
        logging.debug("...")
        return self.bdd.markAsDefinitive()
