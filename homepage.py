import os.path
import cherrypy

from backlog import Backlog
from sprint import Sprint

class HomePage:

    @cherrypy.expose
    def index(self):
        return open(os.path.join('./public', u'index.html'))

root = HomePage()
root.backlog = Backlog()
root.sprints = Sprint()

conf = os.path.join(os.path.dirname(__file__), 'proyecto.conf')

if __name__ == '__main__':
    cherrypy.quickstart(root, config=conf)