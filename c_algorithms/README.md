30/Mayo/2021

Se instalo pipenv para controlar mejor el ambiente del pryecto y evitar el que cuando se actualiza la version de python el proyecto deja de funcionar.

Fue necesario instalar todos los modulos.

Y para instalar el ha_bdd.o es necesario ejecutar el script ./c_algorithms/compile.sh, y el archivo generado se copia a ./processBDD, y entonces ya se puede ejecutar pipenv install ./processBDD/ha_bdd.o

# =========================================================

Para que funcione este proyecto de manera correcta, es necesario generar el archivo core.o el cual se incluye como dependencia en el archivo generado por Cython.

La instruccion a ejecutar para generar el archivo core.o es:
g++ -fPIC -O0 -fno-inline -Wall -g -I"/home/osoto/developing/buddy-2.4" -I"." -c -o "core.o" "core.c"

En el archivo ha_bdd.pyx es necesario agregar la siguiente linea a inicio
# cython: c_string_type=unicode, c_string_encoding=ascii
para que se haga la conversion automatica de los strings entre Python y C++
https://cython.readthedocs.io/en/latest/src/tutorial/strings.html#accepting-strings-from-python-code

Cuando la versión de python cambia, es necesario reinstalar todos los módulos.
Si por alguna razón, el python nuevo no tiene el pip, se puede instalar con el script easy_install
Ej. python3.8 /usr/lib/python3.8/site-packages/easy_install.py pip
