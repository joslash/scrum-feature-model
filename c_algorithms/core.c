#include <core.h>
#include <cstring>
#include <iostream>
#include <fstream>

using namespace std;

void dead_features(int *var_high, int *var_low, int num_var, char *arr_features)
{
	int num_dead, num_core;
	string core_features;
	string dead_features;

	num_dead = 0;
	num_core = 0;

	for (int i = 0; i < num_var; ++i)
	{
		cout << "[" << i << "]"
			 << "var_high " << var_high[i] << ", var_low " << var_low[i] << endl;
		if (var_high[i] != var_low[i])
		{
			if (var_high[i] && !var_low[i])
			{
				num_core++;
				core_features += std::to_string(i) + ",";
				cout << "Core [" << i << "] " << num_core << endl;
			}
			else
			{
				if (!var_high[i] && var_low[i])
				{
					num_dead++;
					dead_features += std::to_string(i) + ",";
					cout << "Dead [" << i << "] " << num_dead << endl;
				}
			}
		}
	}

	cout << "Number of core features : " << num_core << endl;
	cout << "Number of dead features : " << num_dead << endl;

	ofstream file;
	file.open("results.features");

	if (!file)
	{
		cout << "Error in opening file " << endl;
	}

	cout << "Core: " << core_features << endl;
	cout << "Dead: " << dead_features << endl;

	file << "Core: " << core_features << endl;
	file << "Dead: " << dead_features << endl;

	file.close();
} //end dead_features

int get_dependencies_conflicts(const bdd r, int *vars_low, int *vars_high, int *marks, int *res_node)
{
	int level, level_high, level_low;
	int root;

	root = r.id();
	cout << "[" << r.id() << "]"
		 << "bdd.id: " << r.id() << endl;
	cout << "[" << r.id() << "]"
		 << "marks[root]: " << marks[root] << endl;

	if (!marks[root])
	{
		cout << "[" << r.id() << "]"
			 << "!marks[root] " << endl;
		marks[root] = !marks[root];

		//bdd_var2level. Fetch the level of a specific bdd variable.
		//bdd_var. Gets the variable labeling the bdd.
		cout << "[" << r.id() << "]"
			 << "bdd_var (" << r.id() << "): / Variable labeling " << bdd_var(r) << endl;

		level = bdd_var2level(bdd_var(r));
		cout << "[" << r.id() << "]"
			 << "level(bdd_var): " << level << endl;

		//bdd_high. Gets the true branch of a bdd.
		if (bdd_high(r).id() == 1)
		{
			cout << "[" << r.id() << "]"
				 << "bdd_high == 1"
				 << ", high " << bdd_high(r).id() << endl;
			cout << "[" << r.id() << "]"
				 << " level " << level << endl;
			vars_high[level] = 1;

			//bdd_varnum. Returns the number of defined variables. This function returns the number of variables defined by a call to bdd_setvarnum.
			for (int i = level + 1; i < bdd_varnum(); ++i)
			{
				cout << "[" << r.id() << "]"
					 << " vars_high[" << i << "] = 1 " << endl;
				vars_high[i] = 1;
				cout << "[" << r.id() << "]"
					 << " vars_low[" << i << "] = 1 " << endl;
				vars_low[i] = 1;
			}
			res_node[root] = 1;
		}
		else
		{
			//bdd_high. Gets the true branch of a bdd.
			if (bdd_high(r).id() != 0)
			{
				cout << "[" << r.id() << "]"
					 << "bdd_high != 0"
					 << ", high " << bdd_high(r).id() << endl;

				res_node[root] = get_dependencies_conflicts(bdd_high(r), vars_low, vars_high, marks, res_node);
				cout << "[" << r.id() << "]: (bdd_high)"
					 << "res_node[root] " << res_node[root] << endl;

				if (res_node[root])
				{
					cout << "[" << r.id() << "]"
						 << " level " << level << endl;
					vars_high[level] = 1;
					//bdd_var2level. Fetch the level of a specific bdd variable.
					//bdd_var. Gets the variable labeling the bdd.
					//bdd_high. Gets the true branch of a bdd.
					level_high = bdd_var2level(bdd_var(bdd_high(r)));
					cout << "[" << r.id() << "]"
						 << ", level_high " << level_high << ", level " << level << endl;

					for (int i = level + 1; i < level_high; ++i)
					{
						cout << "[" << r.id() << "]"
							 << " vars_high[" << i << "] = 1 " << endl;
						vars_high[i] = 1;
						cout << "[" << r.id() << "]"
							 << " vars_low[" << i << "] = 1 " << endl;
						vars_low[i] = 1;
					}
				}
			}
		}
		//bdd_low. Gets the false branch of a bdd.
		if (bdd_low(r).id() == 1)
		{
			cout << "[" << r.id() << "]"
				 << "bdd_low == 1"
				 << ", low " << bdd_low(r).id() << endl;

			cout << "[" << r.id() << "]"
				 << " level " << level << endl;
			vars_low[level] = 1;
			//bdd_varnum. Returns the number of defined variables. This function returns the number of variables defined by a call to bdd_setvarnum.
			for (int i = level + 1; i < bdd_varnum(); ++i)
			{
				cout << "[" << r.id() << "]"
					 << " vars_high[" << i << "] = 1 " << endl;
				vars_high[i] = 1;
				cout << "[" << r.id() << "]"
					 << " vars_low[" << i << "] = 1 " << endl;
				vars_low[i] = 1;
			}
			res_node[root] = 1;
		}
		else
		{
			if (bdd_low(r).id() != 0)
			{
				cout << "[" << r.id() << "]"
					 << "bdd_low != 0"
					 << ", low " << bdd_low(r).id() << endl;

				//bdd_low. Gets the false branch of a bdd.
				res_node[root] = get_dependencies_conflicts(bdd_low(r), vars_low, vars_high, marks, res_node);
				cout << "[" << r.id() << "]: (bdd_low)"
					 << "res_node[root] " << res_node[root] << endl;

				if (res_node[root])
				{
					cout << "[" << r.id() << "]"
						 << " level " << level << endl;
					vars_low[level] = 1;
					//bdd_var2level. Fetch the level of a specific bdd variable.
					//bdd_var. Gets the variable labeling the bdd.
					//bdd_low. Gets the false branch of a bdd.
					level_low = bdd_var2level(bdd_var(bdd_low(r)));
					cout << "[" << r.id() << "]"
						 << "level_low " << level_low << endl;

					for (int i = level + 1; i < level_low; ++i)
					{
						cout << "[" << r.id() << "]"
							 << " vars_high[" << i << "] = 1 " << endl;
						vars_high[i] = 1;
						cout << "[" << r.id() << "]"
							 << " vars_low[" << i << "] = 1 " << endl;
						vars_low[i] = 1;
					}
				}
			}
		}
	}

	return res_node[root];
} //end get_dependencies_conflicts
