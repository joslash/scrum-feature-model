#ifndef HERADIOS_APPROACH_H
#define HERADIOS_APPROACH_H

namespace tesis {
    class HA_BDD {
        public:
            HA_BDD();
            ~HA_BDD();
            int bdd_main(char* file_name, char arrFeature[]);
        private:
            double timeval_diff(struct timeval *a, struct timeval *b);
    };
}

#endif