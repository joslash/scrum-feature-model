cdef extern from "heradios_approach.cpp":
    pass

cdef extern from "string" namespace "std":
    cdef cppclass string:
        char* c_str()
        string (char *)

cdef extern from "heradios_approach.h" namespace "tesis":
    cdef cppclass HA_BDD:
        HA_BDD() except +
        int bdd_main(char* file_name, char arrFeature[])