# distutils: language = c++
# cython: c_string_type=unicode, c_string_encoding=ascii

from heradios_approach cimport HA_BDD
from libcpp.string cimport string

cdef class Py_HA_BDD:
    cdef HA_BDD.HA_BDD _c_ha_bdd

    def __cinit__(self):
        self._c_ha_bdd = HA_BDD.HA_BDD()

    def process_bdd(self, char* filename, char arrFeature[]):
        self._c_ha_bdd.bdd_main(filename, arrFeature)