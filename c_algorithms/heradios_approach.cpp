using namespace std;
#include <bdd.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <core.h>
#include <heradios_approach.h>

namespace tesis {
  const int INITBDDNODES       = 50000;
  const int INITBDDCACHE       = 10000000;
  const int INITBDDMAXINCREASE = 30000;

  //Default constructor
  HA_BDD::HA_BDD () {}

  //Destructor
  HA_BDD::~HA_BDD () {}

  /* return "a - b" in seconds */
  double HA_BDD::timeval_diff(struct timeval *a, struct timeval *b)
  {
    return
      (double)(a->tv_sec + (double)a->tv_usec/1000000) -
      (double)(b->tv_sec + (double)b->tv_usec/1000000);
  }//time_diff

  int HA_BDD::bdd_main(char* file_name, char arrFeature[]) {
    /* Initilize buddy */
    bdd_init(INITBDDNODES,INITBDDCACHE); //to initialize the BDD package. The nodenum parameter sets the initial number of BDD nodes and cachesize sets the size of the caches used for the BDD operators (not the unique node table)
    bdd_setmaxincrease(INITBDDMAXINCREASE); //to adjust how BuDDy resizes the node table.

    struct timeval t_ini, t_fin;
    double secs;

    bdd solutionSpace;

    int core, dead, num_test;
    int *var_low, *var_high;
    int *marks, *res_node;
    int bddnodesize = INITBDDNODES;

    //string file_name;

    //cout << "Por favor, ingresa el archivo: ";
    //cin >> file_name;

    ifstream mybdd(file_name);

    if(!mybdd) {
      cerr << "Archivo " << file_name << " no encontrado!" << endl;
    }

    cout << "Archivo " << file_name << endl;

    int rvalue = bdd_fnload((char*)file_name, solutionSpace);
    if(rvalue == 0){
      cout << "Se cargo con exito el archivo BDD " << rvalue << endl;
    }

    cout << "Creating bdd file " << endl;

    bdd_fnsave((char *) "saved-original.bdd", solutionSpace);
    
    cout << "bdd file created succesfully " << endl;

    //If all variables should be added as single variable blocks then bdd_varblockall can be used instead of doing it manually.
    bdd_varblockall();
    //Dynamic variable reordering can be done using the functions bdd_reorder(int method) (bd_reorder) and bdd_autoreorder(int method)
    bdd_reorder(BDD_REORDER_SIFT);

    //Initialize time
    gettimeofday(&t_ini, NULL);

    //bdd_varnum. Returns the number of defined variables. This function returns the number of variables defined by a call to bdd_setvarnum.
    cout << "bdd_varnum result " << bdd_varnum() << endl;

    var_low = (int*)calloc(bdd_varnum(), sizeof(int));
    //cout << "Number of defined variables. var_high: " << var_high << endl;

    //bdd_varnum. Returns the number of defined variables. This function returns the number of variables defined by a call to bdd_setvarnum.
    var_high = (int*)calloc(bdd_varnum(), sizeof(int));
    //cout << "Number of defined variables. var_high: " << var_high << endl;

    cout << "bdd node size " << bddnodesize << endl;
    marks = (int*)calloc(bddnodesize, sizeof(int));
    //cout << "bddnodesize " << bddnodesize << ", marks " << marks << endl;

    res_node = (int*)calloc(bddnodesize, sizeof(int));
    //cout << "res_node " << res_node << endl;

    get_dependencies_conflicts(solutionSpace, var_low, var_high, marks, res_node);

    dead_features(var_high, var_low, bdd_varnum(), arrFeature);

    gettimeofday(&t_fin, NULL);
    secs = timeval_diff(&t_fin, &t_ini);

    cout << "Time: " << secs << " seconds." << endl;

    cout << "Creating dot file " << endl;

    bdd_fnprintdot((char *) "data.dot", solutionSpace);

    cout << "dot file created succesfully!" << endl;
    
    cout << "Creating bdd file " << endl;

    bdd_fnsave((char *) "saved.bdd", solutionSpace);
    
    cout << "bdd file created succesfully " << endl;

    bdd_done();

    return 0;
  }
    
};
