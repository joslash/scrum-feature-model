rm -rf ./build ./c_algorithms ha_bdd.cpython* core.o
mkdir ./build
g++ -fPIC -O0 -fno-inline -Wall -g -I"/home/osoto/developing/buddy-2.4" -I"." -c -o "build/core.o" "core.c"
pipenv run python setup.py build_ext --inplace
#cp -fvr ./build/ha_bdd.cpython*.so ../processBDD/ha_bdd.so
