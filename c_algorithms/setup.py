from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

extensions = [
    #Extension("primes", ["primes.pyx"],
    #    include_dirs=[...],
    #    libraries=[...],
    #    library_dirs=[...]),
    # Everything but primes.pyx is included here.
    Extension("*", ["*.pyx"],
        include_dirs=['.'],
        libraries=['bdd'],
        library_dirs=['/home/osoto/developing/buddy-2.4'],
        extra_objects=['build/core.o']),
]

setup(ext_modules=cythonize(extensions))

#https://docs.python.org/2/distutils/apiref.html#distutils.core.Extension
#http://docs.cython.org/en/latest/src/userguide/source_files_and_compilation.html